﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace homework_5
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            var initialMapLocation = MapSpan.FromCenterAndRadius(new Position(43.1365168, 6.0075194), Distance.FromMiles(3));
            map.MoveToRegion(initialMapLocation);
            
            Pin pin = new Pin
            {
                Position = new Position(43.141027, 6.015781),
                Label = "Grand Var",
                Address = "Centre Commercial GRAND VAR, Ctre Grand Var, 83160 La Valette-du-Var",
                Type = PinType.Place
            };
            pin.MarkerClicked += async (s, args) =>
            {
                args.HideInfoWindow = true;
                string pinName = ((Pin)s).Label;
                string pinAddress = ((Pin)s).Address;
                await DisplayAlert(pinName, pinAddress, "Ok");
            };
            map.Pins.Add(pin);

            Pin pin2 = new Pin
            {
                Position = new Position(43.137194, 6.004766),
                Label = "Avenue 83",
                Address = "300 Avenue de l'Université, 83160 La Valette-du-Var",
                Type = PinType.Place
            };
            pin2.MarkerClicked += async (s, args) =>
            {
                args.HideInfoWindow = true;
                string pinName = ((Pin)s).Label;
                string pinAddress = ((Pin)s).Address;
                await DisplayAlert(pinName, pinAddress, "Ok");
            };
            map.Pins.Add(pin2);

            Pin pin3 = new Pin
            {
                Position = new Position(43.132151, 6.018947),
                Label = "Lycée Le Coudon",
                Address = "65 Avenue Henri Toulouse Lautrec, 83130 La Garde",
                Type = PinType.Place
            };
            pin3.MarkerClicked += async (s, args) =>
            {
                args.HideInfoWindow = true;
                string pinName = ((Pin)s).Label;
                string pinAddress = ((Pin)s).Address;
                await DisplayAlert(pinName, pinAddress, "Ok");
            };
            map.Pins.Add(pin3);

            Pin pin4 = new Pin
            {
                Position = new Position(43.133844, 6.006341),
                Label = "Home",
                Address = "57 Allée du Fenouil",
                Type = PinType.Place
            };
            pin4.MarkerClicked += async (s, args) =>
            {
                args.HideInfoWindow = true;
                string pinName = ((Pin)s).Label;
                string pinAddress = ((Pin)s).Address;
                await DisplayAlert(pinName, pinAddress, "Ok");
            };
            map.Pins.Add(pin4);



        }

        void StreetViewClicked(object sender, EventArgs args)
        {
            map.MapType = MapType.Street;
        }

        void SatelliteViewClicked(object sender, EventArgs args)
        {
            map.MapType = MapType.Satellite;
        }

        void HybridViewClicked(object sender, EventArgs args)
        {
            map.MapType = MapType.Hybrid;
        }

        void OnPickerSelectedIndexChanged(object sender, EventArgs e)
        {
            var picker = (Picker)sender;
            int selectedIndex = picker.SelectedIndex;

            if (selectedIndex != -1)
            {
                if ((string)picker.ItemsSource[selectedIndex] == "Home")
                {
                    var HomeLocation = MapSpan.FromCenterAndRadius(new Position(43.133844, 6.006341), Distance.FromMiles(1));
                    map.MoveToRegion(HomeLocation);
                }

                if ((string)picker.ItemsSource[selectedIndex] == "Grand Var")
                {
                    var Grand_VarLocation = MapSpan.FromCenterAndRadius(new Position(43.141027, 6.015781), Distance.FromMiles(1));
                    map.MoveToRegion(Grand_VarLocation);
                }

                if ((string)picker.ItemsSource[selectedIndex] == "L'avenue 83")
                {
                    var avenue_83Location = MapSpan.FromCenterAndRadius(new Position(43.137194, 6.004766), Distance.FromMiles(1));
                    map.MoveToRegion(avenue_83Location);
                }
                if ((string)picker.ItemsSource[selectedIndex] == "Lycée Le Coudon")
                {
                    var lyceeLocation = MapSpan.FromCenterAndRadius(new Position(43.132151, 6.018947), Distance.FromMiles(1));
                    map.MoveToRegion(lyceeLocation);
                }

                
            }
        }
    }
}
